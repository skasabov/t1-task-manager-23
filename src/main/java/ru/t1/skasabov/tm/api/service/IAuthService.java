package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @NotNull
    Boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

    void checkRoles(@Nullable Role[] roles);

}
